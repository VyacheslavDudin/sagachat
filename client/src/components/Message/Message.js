import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import './Message.css';
import { deleteMessageAction, likeMessageAction } from './../../containers/Chat/action';

export function Message(props) {
    const { message, currentUser, onEdit, likesCount, deleteMessage, likeMessage } = props;
    let {avatar, text, user, msgId, createdAt} = message;
    createdAt = (new Date(createdAt)).toLocaleTimeString();
    if (currentUser.user !== user) {
        return (
            <div className="message">
                <div className="message-main">
                    <img className="avatar" src={avatar} alt="avatar"/>
                    <div className="message-body">
                        <span className="message-author">{user}</span>
                        <p className="message-text">{text}</p>
                    </div>
                </div>
                <div className="message-toolbar">
                    <i className="far fa-thumbs-up"onClick={() => likeMessage(msgId)}>{likesCount}</i>
                    <span>{createdAt}</span>
                </div>      
            </div>
        );
    }
    else {
        return (
            <div className="message message_right">
                <div className="message-main">
                    <div className="message-body">
                        <p className="message-text">{text}</p>
                    </div>
                </div>
                <div className="message-toolbar">
                    <i className="far fa-thumbs-up">{likesCount}</i>
                    <i className="far fa-edit" onClick={() => onEdit(msgId, text)}></i>
                    <i className="far fa-trash-alt" onClick={() => deleteMessage(msgId)}></i>
                    <span>{createdAt}</span>
                </div>      
            </div>
        );
    }
}

const actions = { 
    deleteMessage: deleteMessageAction, 
    likeMessage: likeMessageAction
};

function mapStateToProps(state) {
    return {
      messages: state.messages,
      currentUser: state.currentUser
    }
}

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
  
export default connect(mapStateToProps, mapDispatchToProps)(Message);