import { all } from 'redux-saga/effects';
import userPageSagas from './../userPage/sagas';
import usersSagas from './../users/sagas';
import messagesSagas from './../containers/Chat/saga';

export default function* rootSaga() {
    yield all([
        userPageSagas(),
        usersSagas(),
        messagesSagas()
    ]);
};