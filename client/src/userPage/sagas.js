import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import {FETCH_USER, FETCH_USER_SUCCESS} from './actionTypes';
import { CHANGE_SPINNER_ACTIVE } from '../users/actionTypes';
import { apiPath } from "./../config/config";


export function* fetchUser(action) {
    try {
        const user = yield call(axios.get, `${apiPath}/users/${action.payload.id}`);
        yield put({type: FETCH_USER_SUCCESS, payload: {userData: user.data}});
    } catch(error) {
        alert(`Fetch user error: ${error.message}`);
    }
}

function* watchFetchUser() {
    yield takeEvery(FETCH_USER, fetchUser)
}

export default function* userPageSagas() {
    yield all([
       watchFetchUser()
    ]);
}


