import {
    FETCH_USER_SUCCESS,
    FETCH_USER
  } from './actionTypes';

export const fetchUserSuccessAction = userData => ({
    type: FETCH_USER_SUCCESS,
    payload: {
        userData
    }
});

export const fetchUserAction = id => ({
    type: FETCH_USER,
    payload: {
        id
    }
});
