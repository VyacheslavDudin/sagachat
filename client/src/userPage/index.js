import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from './actions';
import { addUserAction, editUserAction } from './../users/actions';
import './userPage.css';

class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultUserData();
        this.onCancel = this.onCancel.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onChangeData = this.onChangeData.bind(this);
    }

    componentDidMount() {
        if (this.props.match.params[0]) {
            this.props.fetchUserAction(this.props.match.params[0]);
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            ...nextProps.userData
        };
    }

    onCancel() {
        this.setState(this.getDefaultUserData());
        this.props.history.push('/users')
    }

    onSave() {
        if (this.state.id) {
            this.props.editUserAction(this.state.id, this.props.userData);
        } else {
            this.props.addUser(this.state);
        }
        this.setState(this.getDefaultUserData());
        this.props.history.push('/users');
    }

    onChangeData(e, keyword) {
        const value = e.target.value;
        this.setState(
            {
                ...this.state,
                [keyword]: value
            }
        );
    }

    getDefaultUserData() {
        return {
            login: '',
            password: '',
            name: ''
        }
    }

    render() {
        return(
            <div className="modal">
                <div className="modal-dialog">
                    <div className="form">
                        <h2 className="form__title">Editing form</h2> 
                        <div className="name-wrapper">
                            <label htmlFor="name">Name: </label>
                            <input type="text" id="name-input" onInput={(e)=>this.onChangeData(e, 'name')} defaultValue={this.props.userData.name} autoComplete="false"/>
                        </div>
                        
                        <div className="login-wrapper">
                            <label htmlFor="login">Login: </label>
                            <input type="text" id="login-input" onInput={(e)=>this.onChangeData(e, 'login')} defaultValue={this.props.userData.login} autoComplete="false"/>
                        </div>

                        <div className="password-wrapper">
                            <label htmlFor="password">Password: </label>
                            <input type="password" id="password-input" onInput={(e)=>this.onChangeData(e, 'password')} defaultValue={this.props.userData.password} autoComplete="false"/>
                        </div>
                        <button className="save-btn" onClick={() => this.onSave()}>Save</button>
                    </div>
                </div>
            </div>
        )
    };
}

const mapStateToProps = state => {
    return {
        userData: state.userPage.userData
    }
};

const mapDispatchToProps = {
    ...actions,
    addUserAction,
    editUserAction
};

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);