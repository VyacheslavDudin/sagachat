import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from './actions';
import Spinner from './../components/Spinner/Spinner';
import './users.css';

class Users extends Component {
    constructor(props) {
        super(props);
        this.onDeleteUser = this.onDeleteUser.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.state = this.getDefaultUserData();
    }

    componentDidMount() {
        this.props.fetchUsersAction();
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        const { userData, users, isSpinnerActive } = nextProps;
            return {
                ...userData,
                users,
                isSpinnerActive
            };
    }

    onDeleteUser(id) {
        this.props.deleteUserAction(id);
        this.setState(this.getDefaultUserData());
    }

    onEdit(id) {
        this.setState(this.getDefaultUserData());
        this.props.history.push(`user-page/${id}`);
    }

    getDefaultUserData() {
        return {
            login: '',
            password: '',
            name: ''
        }
    }

    render() {
        let userList = null;
        if(this.state.users.length !== 0) {
            userList = this.props.users.map(user => (
            <div className="user">
                <div className="user-name">{user.name}</div>
                <button className="edit-btn" onClick={() => this.onEdit(user.id)}>Edit</button>
                <button className="delete-btn" onClick={() => this.onDeleteUser(user.id)}>Delete</button>
            </div> ));
        }   
        return(
            <div className="users-wrapper">
                <div className="users">
                    {this.props.isSpinnerActive ? <Spinner/> : userList}
                </div>
            </div>
        )
    };
}

const mapStateToProps = state => {
    return {
        userData: state.userPage.userData,
        users: state.users,
        isSpinnerActive: state.isSpinnerActive
    }
};

const mapDispatchToProps = {
    ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);