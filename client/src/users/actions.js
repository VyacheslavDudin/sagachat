import {
    FETCH_USERS_SUCCESS,
    FETCH_USERS,
    DELETE_USER,
    UPDATE_USER,
    ADD_USER,
    CHANGE_SPINNER_ACTIVE
  } from './actionTypes';
  
export const addUserAction = data => ({
    type: ADD_USER,
    payload: {
        data
    }
});

export const editUserAction = (id, data) => ({
    type: UPDATE_USER,
    payload: {
        id,
        data
    }
});

export const deleteUserAction = id => ({
    type: DELETE_USER,
    payload: {
        id
    }
});

export const fetchUsersAction = () => ({
    type: FETCH_USERS
});

export const changeSpinnerActive = isSpinnerActive => ({
    type: CHANGE_SPINNER_ACTIVE,
    payload: {
        isSpinnerActive
    }
});


export const fetchUsersSuccessAction = users => ({
    type: FETCH_USERS_SUCCESS,
    payload: {
        users
    }
});
