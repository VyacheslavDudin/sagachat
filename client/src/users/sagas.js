import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { ADD_USER, UPDATE_USER, DELETE_USER, FETCH_USERS, FETCH_USERS_SUCCESS } from './actionTypes';
import { apiPath } from './../config/config'; 

export function* fetchUsers() {
    try {
        const users = yield call(axios.get, `${apiPath}/users`);
        yield put({type: FETCH_USERS_SUCCESS, payload: { users: users.data }});
    } catch(error) {
        alert(`Fetch users error: ${error.message}`);
    }
}

function* watchFetchUsers() {
    yield takeEvery(FETCH_USERS, fetchUsers);
}

export function* addUser(action) {
    const newUser = {...action.payload.data, id: action.payload.id};

    try {
        yield call(axios.post, `${apiPath}/users`, newUser);
        yield put({type: FETCH_USERS});
    } catch (error) {
        alert(`Create user error: ${error.message}`);
    }
}

function* watchAddUser() {
    yield takeEvery(ADD_USER, addUser);
}

export function* updateUser(action) {
    const id = action.payload.id;
    const updatedUser = {...action.payload.data};

    try {
        yield call(axios.put, `${apiPath}/users/${id}`, updatedUser);
        yield put({type: FETCH_USERS});
    } catch (error) {
        alert(`Update user error: ${error.message}`);
    }
}

function* watchUpdateUser() {
    yield takeEvery(UPDATE_USER, updateUser);
}

export function* deleteUser(action) {
    const id = action.payload.id;
    try {
        yield call(axios.delete, `${apiPath}/users/${id}`);
        yield put({type: FETCH_USERS});
    } catch (error) {
        alert(`Delete user error: ${error.message}`);
    }
}

function* watchDeleteUser() {
    yield takeEvery(DELETE_USER, deleteUser);
}

export default function* usersSagas() {
    yield all([
        watchFetchUsers(),
        watchUpdateUser(),
        watchAddUser(),
        watchDeleteUser()
    ]);
};