import React from 'react';
import './Login.css';

export function Login (props) {
    return (
        <div className="login">
            <form className="login-form">
                <h2 className="login-form__title">Login form</h2> 
                <div className="login-wrapper">
                    <label htmlFor="login">Login: </label>
                    <input type="text" id="login-input"/>
                </div>
                
                <div className="password-wrapper">
                    <label htmlFor="password">Password: </label>
                    <input type="password" id="password-input"/>
                </div>
                <button className="login-btn">Log in</button>
            </form>
        </div>
    );
}