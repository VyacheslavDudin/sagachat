import React from 'react';
import './MessageEditor.css';

export function MessageEditor (props) {
    return (
        <div className="editor">
            <div className="editor-main">
                <h2 className="editor-title">Edit message</h2>
                <textarea className="editor-textarea" placeholder="Edit message"></textarea>
                <div className="edit-buttons">
                    <button className="ok-btn">OK</button>
                    <button className="cancel-btn">Cancel</button>
                </div>
            </div>
        </div>
    );
}