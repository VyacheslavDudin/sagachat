import React, { Component } from 'react';
import './Chat.css';
import Header from './../../components/Header/Header';
import Spinner from './../../components/Spinner/Spinner';
import MessageList from '../MessageList/MessageList';

export class Chat extends Component {
  constructor(props) {
    super(props);
    this.apiPath = 'https://edikdolynskyi.github.io/react_sources/messages.json';
    // Since it does not affect any other components and it`s allowed to use local state
    // instead of clogging global state, I use local state for 'isLoaded'
    this.state = {
      isMessageListLoaded: false
    }
  }
  
  
  componentDidMount() {
    this.setState({isMessageListLoaded: true});
  }

  render() {
    const { isMessageListLoaded: isLoaded } = this.state;

    return (
      <>
        <div className="logo">
          <img className="logo__img" src="./logo192.png" alt="logo"/>
          <span className="logo__text">Logo</span>
        </div>
        <div className="chat">
            {
            isLoaded 
            ?
              <>
                <Header className="chat-header"/>
                <MessageList/>
              </>
            : <Spinner/>
            }
        </div>
        <footer>&copy; 2020 Dudin Vyacheslav for <a href="https://academy.binary-studio.com/ua/"> Binary Studio Academy</a></footer>
      </>
    );
  }
}
