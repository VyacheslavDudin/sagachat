import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { DELETE_MESSAGE, EDIT_MESSAGE, ADD_MESSAGE, LIKE_MESSAGE, FETCH_MESSAGES_SUCCESS, FETCH_MESSAGES } from './actionTypes';
import { apiPath } from './../../config/config'; 

export function* fetchMessages() {
    try {
        const messages = yield call(axios.get, `${apiPath}/messages`);
        yield put({type: FETCH_MESSAGES_SUCCESS, payload: { messages: messages.data }});
    } catch(error) {
        alert(`Fetch messages error: ${error.message}`);
    }
}

function* watchFetchMessages() {
    yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* addMessage(action) {
    const newMessage = {...action.payload.data, id: action.payload.id};

    try {
        yield call(axios.post, `${apiPath}/messages`, newMessage);
        yield put({type: FETCH_MESSAGES});
    } catch (error) {
        alert(`Create message error: ${error.message}`);
    }
}

function* watchAddMessage() {
    yield takeEvery(ADD_MESSAGE, addMessage);
}

export function* updateMessage(action) {
    const id = action.payload.id;
    const updatedMessage = {...action.payload.data};

    try {
        yield call(axios.put, `${apiPath}/messages/${id}`, updatedMessage);
        yield put({type: FETCH_MESSAGES});
    } catch (error) {
        alert(`Update Message error: ${error.message}`);
    }
}

function* watchUpdateMessage() {
    yield takeEvery(EDIT_MESSAGE, updateMessage);
}

export function* deleteMessage(action) {
    const id = action.payload.id;
    try {
        yield call(axios.delete, `${apiPath}/messages/${id}`);
        yield put({type: FETCH_MESSAGES});
    } catch (error) {
        alert(`Delete message error: ${error.message}`);
    }
}

function* watchDeleteMessage() {
    yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

export default function* messagesSagas() {
    yield all([
        watchFetchMessages(),
        watchUpdateMessage(),
        watchAddMessage(),
        watchDeleteMessage()
    ]);
};