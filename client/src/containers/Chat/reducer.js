import {
    FETCH_MESSAGES_SUCCESS,
    LIKE_MESSAGE
  } from './actionTypes';
  
  export default function reducer(state = {}, action) {
    switch (action.type) {
      case FETCH_MESSAGES_SUCCESS:
        return [...action.payload.messages];
      case LIKE_MESSAGE:
        const {userId: authorId} = state.currentUser;
        const {id} = action;

        if ((state.likeRepository || []).some(like => like.userId === authorId && like.id === id)) {
            return {
                ...state,
                likeRepository: (state.likeRepository || []).filter(like => like.id !== id || like.userId !== authorId)
            };
        }
        else {
            return {
                ...state,
                likeRepository: [...(state.likeRepository || []), {id, userId: authorId}]
            };
        }
      default:
        return state;
    }
  };
  