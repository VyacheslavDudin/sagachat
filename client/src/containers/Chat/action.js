import {
    EDIT_MESSAGE,
    LIKE_MESSAGE,
    ADD_MESSAGE,
    DELETE_MESSAGE
  } from './actionTypes';
  
export const addMessageAction = message => ({
    type: ADD_MESSAGE,
    message
});

export const editMessageAction = (id, messageText) => ({
    type: EDIT_MESSAGE,
    id,
    messageText
});

export const deleteMessageAction = id => ({
    type: DELETE_MESSAGE,
    id
});

export const likeMessageAction = id => ({
    type: LIKE_MESSAGE,
    id
});
