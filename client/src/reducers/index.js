import { combineReducers } from 'redux'
import users from './../users/reducer';
import userPage from './../userPage/reducer';
import messages from './../containers/Chat/reducer';


export default combineReducers({
  users,
  userPage,
  messages
});