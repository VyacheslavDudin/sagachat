import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import './index.css';
import { Chat } from './containers/Chat/Chat';
import { Login } from './containers/Login/Login';
import configureStore from './configureStore';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import { MessageEditor } from './containers/MessageEditor/MessageEditor';
import userPage from './userPage';
import users from './users';

const store = configureStore();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/" component={Chat}/>
          <Route path="/login" component={Login}/>
          <Route path="/editor" component={MessageEditor}/>
          <Route path="/user-page/*" component={userPage}/>
          <Route path="/users" component={users}/>
        </Switch>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);