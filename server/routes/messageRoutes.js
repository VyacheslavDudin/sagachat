const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function (req, res, next) {
    req.result = MessageService.getAll();
    req.entityName = 'Message';
    next();
}, responseMiddleware);

router.get('/:id', function (req, res, next) {
    req.result = MessageService.search(req.params.id);
    req.entityName = 'Message';
    next();
}, responseMiddleware);


router.post('/', function (req, res, next) {
    req.entityName = 'Message';
    // if(req.isValid === true) 
    //     req.result = MessageService.createMessage(req.newMessage);
    MessageService.createMessage({text:"20text"});
    next();
}, responseMiddleware);


router.put('/:id', function (req, res, next) {
    req.entityName = 'Message';
    if(req.isValid === true) 
        req.result = MessageService.updateMessage(req.params.id, req.newMessageParams);
    next();
}, responseMiddleware);

router.delete('/:id', function (req, res, next) {
    req.entityName = 'Message';
    req.result = MessageService.deleteMessage(req.params.id);
    next();
}, responseMiddleware);

module.exports = router;