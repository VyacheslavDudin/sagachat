const { Router } = require('express');
const UserService = require('../services/userService');
// const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function (req, res, next) {
    req.result = UserService.getAll();
    req.entityName = 'User';
    next();
}, responseMiddleware);

router.get('/:id', function (req, res, next) {
    req.result = UserService.search(req.params.id);
    req.entityName = 'User';
    next();
}, responseMiddleware);


router.post('/', function (req, res, next) {
    let nu = {name: "Skot"};
    req.entityName = 'User';
    req.result = UserService.createUser({...req.params.userData});
    next();
}, responseMiddleware);


router.put('/:id', function (req, res, next) {
    req.entityName = 'User';
    req.result = UserService.updateUser(req.params.id, req.params.data);
    next();
}, responseMiddleware);

router.delete('/:id', function (req, res, next) {
    req.entityName = 'User';
    req.result = UserService.deleteUser(req.params.id);
    next();
}, responseMiddleware);

module.exports = router;