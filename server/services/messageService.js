const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
    search(search) {
        const item = MessageRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getAll() {
        const allItems = MessageRepository.getAll();
        return Object.keys(allItems).length === 0 
            ? null
            : allItems;
    }

    createMessage(newMessage) {
        return MessageRepository.create(newMessage);
    }

    updateMessage(id, dataToUpdate) {
        return MessageRepository.update(id, dataToUpdate);
    }

    deleteMessage(id) {
        return MessageRepository.delete(id);
    }
}

module.exports = new MessageService();