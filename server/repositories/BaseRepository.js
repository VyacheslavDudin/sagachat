import { dbAdapter } from '../config/db';
import { v4 } from 'uuid';


class BaseRepository {
    constructor(collectionName) {
        this.dbContext = dbAdapter.get(collectionName);
        this.collectionName = collectionName;
    }

    generateId() {
        return v4();
    }

    getAll() {
        return this.dbContext.value() === undefined 
        ? null
        : this.dbContext.value();
    }

    getOne(search) {
        let item;
        if(Object.prototype.toString.call(search) === '[object Object]'){
            let {email, password} = search;
            item = this.dbContext.find(it => it.email === email && it.password === password);
        }
        else  item = this.dbContext.find(it => it.id === search);
        return item.value() === undefined
        ? null
        : item.value();
    }

    create(data) {
        data.createdAt = new Date();
        const list = this.dbContext.push(data).write();
        return list.find(it => it.id === data.id);
    }

    update(id, dataToUpdate) {
        dataToUpdate.updatedAt = new Date();
        let item = this.dbContext.find({ id });
        return item.value() === undefined
        ? null
        : item.assign(dataToUpdate).write();
    }

    delete(id) {
        let item = this.dbContext.find({ id });
        return item.value() === undefined
        ? null
        : this.dbContext.remove({ id }).write();
    }
}

const _BaseRepository = BaseRepository;
export { _BaseRepository as BaseRepository };