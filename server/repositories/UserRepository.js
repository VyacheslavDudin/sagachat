import { BaseRepository } from './baseRepository';

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }
}

const _UserRepository = new UserRepository();
export { _UserRepository as UserRepository };