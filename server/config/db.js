import { resolve } from 'path';
const dbPath = `${resolve()}/database.json`;

import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';
const adapter = new FileSync(dbPath);

const dbAdapter = low(adapter);

const defaultDb = { users: [], messages: [] };

dbAdapter.defaults(defaultDb).write();

const _dbAdapter = dbAdapter;
export { _dbAdapter as dbAdapter };