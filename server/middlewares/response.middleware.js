const responseMiddleware = (req, res, next) => {
    let error = false;
    let result = req.result;
    let message = '';
   switch(req.method) {
       case 'GET': {
            if(result === null) {
                res.status(404);
                error = true;
                message = `${req.entityName}(s) not found`;
            } 
            break;
       }
       case 'POST': {
            if(req.isValid === false) {
                res.status(400);
                error = true;
                message = `${req.entityName} entity to create is not valid`;
            } else if(res.err) {
                res.status(404);
                error = true;
                message = res.err;
        }  
            break;
       }
       case 'PUT': {
            if(req.isValid === false) {
                res.status(400);
                error = true;
                message = `${req.entityName} entity to update is not valid`;
            }
            else if(result === null) {
                    res.status(404);
                    error = true;
                    message = `${req.entityName} does not exist`;
            } 
            break;
       }
       case 'DELETE': {
            if(result === null) {
                res.status(404);
                error = true;
                message = `${req.entityName} not found`;
            } 
            break;
       }
       default: {
            error = true;
            message = `Invalid query method`;
       }
   }

    if(error) {
        res.send({error, message});
    } 
    else {
        res.status(200).send(JSON.stringify(result));
    }
}

const _responseMiddleware = responseMiddleware;
export { _responseMiddleware as responseMiddleware };